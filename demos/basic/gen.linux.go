// +build generate
package main

//go:generate bash -c "rm curve.gen.go curve.basic.gen.go  || exit 0"
//go:generate go build bitbucket.org/ullutau/curve
//go:generate go install bitbucket.org/ullutau/curve
//go:generate echo "curve installed"
//go:generate ./curve -pkg=main
