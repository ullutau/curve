package curve

// todo: Better error handling and reportig
// todo: Html mode
// todo: What about forced error returning?
// todo: Rewrite parser / add line numbers to output
// todo: Integration with editors (Dummy run?)

import (
	"bytes"
	"fmt"
	"go/format"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func DoDir(pkg, dir string) error {
	items, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}

	var files []string

	for _, item := range items {
		if !item.IsDir() && strings.HasSuffix(item.Name(), ".curve") {
			files = append(files, item.Name())
		}
	}

	return DoFiles(pkg, dir, files...)
}

type resultingFile struct {
	Package   string
	Imports   map[string]interface{}
	Functions []io.Reader
}

func DoFiles(pkg, dir string, files ...string) error {
	var tpls []Template
	for _, file := range files {
		log.Println("Doing file:", file)

		f, err := os.Open(filepath.Join(dir, file))
		if err != nil {
			return err
		}
		defer f.Close()

		tpls_, err := ParseUnit(f)
		if err != nil {
			return err
		}
		tpls = append(tpls, tpls_...)
	}

	//data, _ := json.MarshalIndent(tpls, "", "  ")
	//ioutil.WriteFile("./~curve.ast.debug.json", data, 0777)

	var results = map[string]*resultingFile{}

	for _, tpl := range tpls {
		fnbuf := bytes.NewBuffer([]byte("func "))
		if tpl.Header.Extend != "" {
			fmt.Fprintf(fnbuf, "(%s) ", tpl.Header.Extend)
		}
		fmt.Fprintf(fnbuf, "%s {\n ", tpl.Header.Func)

		for _, block := range tpl.Blocks {
			switch block.Type {
			case Text:
				fmt.Fprintf(fnbuf, "fmt.Fprint(w, %s)\n", strconv.Quote(block.Text))
			case Code:
				fmt.Fprintf(fnbuf, "%s\n", block.Text)
			case CodeAs:
				fmt.Fprintf(fnbuf, "fmt.Fprint(w, %s)\n", block.Text)
			}
		}

		fmt.Fprintf(fnbuf, "}\n")

		{
			if tpl.Header.File == "" {
				tpl.Header.File = "curve.gen.go"
			}
			rf, ok := results[tpl.Header.File]
			if !ok {
				results[tpl.Header.File] = &resultingFile{
					Package: pkg,
					Imports: map[string]interface{}{},
				}
				rf = results[tpl.Header.File]
			}

			rf.Functions = append(rf.Functions, fnbuf)
			for _, imp := range tpl.Header.Import {
				rf.Imports[imp] = nil
			}
		}
	}

	//data, _ = json.MarshalIndent(results, "", "  ")
	//ioutil.WriteFile("./~curve.compose.debug.json", data, 0777)

	for fname, res := range results {
		buf := bytes.NewBuffer([]byte{})

		if len(res.Functions) == 0 {
			continue
		}

		fmt.Fprintf(buf, "package %s\n\nimport (\n\t\"fmt\"\n\t\"io\"", res.Package)

		for imp, _ := range res.Imports {
			fmt.Fprintf(buf, "\n\t\"%s\"", imp)
		}
		fmt.Fprintf(buf, "\n)\n")

		for _, fn := range res.Functions {
			io.Copy(buf, fn)
			fmt.Fprint(buf, "\n")
		}

		data, err := format.Source(buf.Bytes())
		if err != nil {
			return err
		}

		err = ioutil.WriteFile(filepath.Join(dir, fname), data, 0777)
		if err != nil {
			return err
		}
	}

	return nil
}
