package curve

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"regexp"
	"strings"

	"gopkg.in/yaml.v2"
)

type TemplateHeader struct {
	Doc        string // todo: Implement
	Func       string
	Import     []string
	Extend     string
	File       string
	FullDelims bool
}

type Block struct {
	Type int
	Text string
}

type Template struct {
	Header TemplateHeader
	Blocks []Block
}

type TemplateDelimiters struct {
	Header  string
	TplEnd  string
	Code    string
	CodeAs  string
	CodeEnd string
}

const (
	Text   = 1
	Code   = 2
	CodeAs = 3
)

type TemplateReader struct {
	r  *bufio.Reader
	bt int
}

func NewTemplateReader(r io.Reader) *TemplateReader {
	this := &TemplateReader{
		r:  bufio.NewReader(r),
		bt: Text,
	}
	return this
}

func (this *TemplateReader) skipEmpty() error {
	for {
		b, err := this.r.ReadByte()
		if err != nil {
			return err
		}
		if b != '\n' && b != '\r' && b != '\t' && b != ' ' {
			err = this.r.UnreadByte()
			if err != nil {
				return err
			}
			return nil
		}
	}
}

func (this *TemplateReader) readHeader() (*TemplateHeader, *TemplateDelimiters, error) {
	err := this.skipEmpty()
	if err != nil {
		return nil, nil, err
	}

	var firstLine string
	firstLine, err = this.r.ReadString('\n')
	if err != nil {
		return nil, nil, err
	}

	firstLine = strings.Trim(firstLine, " \n\t")
	if firstLine == "" {
		return nil, nil, errors.New(fmt.Sprintf("Empty header"))
	}

	//> If we have a line of 80 similar characters before header - skip it
	if firstLine == strings.Repeat(fmt.Sprintf("%c", firstLine[0]), 80) {
		return this.readHeader()
	}

	var delims []string

	delims = strings.Split(strings.TrimSpace(firstLine), " ! ")
	if len(delims) != 2 || len(delims[0]) < 2 || len(delims[1]) < 2 {
		return nil, nil, errors.New(fmt.Sprintf("Malformed delimiters: %s", firstLine))
	}

	var headerText string
	for {
		var headerLine string

		headerLine, err = this.r.ReadString('\n')
		if err != nil && err != io.EOF {
			return nil, nil, err
		}
		if strings.Trim(headerLine, " \n\t") != firstLine {
			headerText += headerLine
		} else {
			break
		}
		if err == io.EOF {
			break
		}
	}

	var header = &TemplateHeader{}
	if err := yaml.Unmarshal([]byte(headerText), header); err != nil {
		return nil, nil, err
	}

	if err == io.EOF {
		return header, nil, io.EOF
	}

	var dlms = &TemplateDelimiters{
		Header:  firstLine,
		TplEnd:  fmt.Sprint(delims[0], delims[0], " ! ", delims[1], delims[1]),
		CodeEnd: delims[1],
		Code:    delims[0],
		CodeAs:  delims[0],
	}

	if header.FullDelims {
		dlms.CodeAs = dlms.CodeAs + "="
	} else {
		dlms.CodeAs = dlms.CodeAs[:len(dlms.CodeAs)-1] + "="
	}

	return header, dlms, nil
}

func (this *TemplateReader) readBody(dlms *TemplateDelimiters) (body string, err error) {
	for {
		var line string
		line, err = this.r.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				body += line
			}
			return
		}

		if strings.Trim(line, " \n\t") == dlms.TplEnd {
			return
		}
		body += line
	}

	return
}

func ParseUnit(r io.Reader) ([]Template, error) {
	rdr := NewTemplateReader(r)

	var tpls []Template

	cont := true
	for cont { //> Read template
		log.Println(strings.Repeat("=", 80))

		hdr, dlms, err := rdr.readHeader()
		if err != nil {
			if err == io.EOF {
				if hdr == nil {
					return tpls, nil
				} else {
					//> We got only header in template
				}
			} else {
				return nil, err
			}
		}

		var tpl = Template{
			Header: *hdr,
		}

		if err == io.EOF {
			tpls = append(tpls, tpl)
			return tpls, nil
		}

		body, err := rdr.readBody(dlms)
		if err != nil {
			if err == io.EOF {
				cont = false
			} else {
				return nil, err
			}
		}

		err = parseBody(body, &tpl, dlms)
		if err != nil {
			return nil, err
		}

		tpls = append(tpls, tpl)
	}

	return tpls, nil
}

//func perror(err error) {
//	if err != nil {
//		log.Panicln(err)
//	}
//}

// This shit uses REGEXP!!!!!!!!!!!!!!!!!!!
func parseBody(body string, tpl *Template, dlms *TemplateDelimiters) error {
	getParserRx := func(this *TemplateDelimiters) (rx string) {
		var (
			code    string
			codeas  string
			codeend string

			esc = func(c rune) string {
				return fmt.Sprintf(`\%c`, c)
			}
		)

		for _, c := range this.Code {
			code += esc(c)
		}
		for _, c := range this.CodeAs {
			codeas += esc(c)
		}
		for _, c := range this.CodeEnd {
			codeend += esc(c)
		}

		// http://play.golang.org/p/kPBuM8lJ7w
		rx = fmt.Sprintf(`(?Ums)(.*(?P<v>%s.*%s)|.*(?P<v>%s.*%s)|.*\z)+`,
			codeas, codeend, code, codeend)
		return
	}

	rx := regexp.MustCompile(getParserRx(dlms))
	mchs := rx.FindAllStringSubmatch(body, -1)
	for _, m := range mchs {
		if m[2] != "" {
			m[1] = strings.TrimSuffix(m[1], m[2])
			m[2] = strings.TrimPrefix(strings.TrimSuffix(m[2], dlms.CodeEnd), dlms.CodeAs)
			tpl.Blocks = append(tpl.Blocks, Block{
				Type: Text,
				Text: m[1],
			}, Block{
				Type: CodeAs,
				Text: m[2],
			})
		} else if m[3] != "" {
			m[1] = strings.TrimSuffix(m[1], m[3])
			m[3] = strings.TrimPrefix(strings.TrimSuffix(m[3], dlms.CodeEnd), dlms.Code)
			tpl.Blocks = append(tpl.Blocks, Block{
				Type: Text,
				Text: m[1],
			}, Block{
				Type: Code,
				Text: m[3],
			})
		} else {
			tpl.Blocks = append(tpl.Blocks, Block{
				Type: Text,
				Text: m[1],
			})
		}
	}

	return nil
}
