package main

import (
	"flag"
	"log"
	"os"

	"bitbucket.org/ullutau/curve/lib/curve"
)

func main() {
	pkgName := flag.String("pkg", "main", "Package name for generated files")
	flag.Parse()

	wd, err := os.Getwd()
	perror(err)

	perror(curve.DoDir(*pkgName, wd))
}

func perror(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func perrorf(err error, format string, args ...interface{}) {
	if err != nil {
		log.Panicf(format, append([]interface{}{err}, args...)...)
	}
}
