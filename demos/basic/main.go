package main

import (
	"fmt"
	"io"
	"os"
)

type UserObject struct {
	Name  string
	Age   int
	Phone string
}

func (this *UserObject) fmtAge(w io.Writer) {
	fmt.Fprintf(w, "%dyo", this.Age)
}

func main() {
	(&UserObject{
		Name:  "Jim",
		Age:   23,
		Phone: "+74950000000",
	}).Render(os.Stdout)
}
